//
//  AppDelegate.h
//  uiscrollview
//
//  Created by hoho on 14-12-6.
//  Copyright (c) 2014年 thinkcart.net. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

